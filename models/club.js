var mongoose = require('mongoose');
var helpers = require('../core/helpers');
var moment = require('moment');

var schema = new mongoose.Schema({
	name: String,
	date: {type: Date, default: Date.now},
	stats: {type: Object, default: {
		totals: {
			registrations: 0,
			checkins: 0,
			subscriptions: 0,
			money: 0
		},
		money: {}, // by year+month+day (2014.01.03)
		checkins: {}
	}}
});

schema.statics.newClient = function(id, callback) {
	var date = moment().format('YYYYMMDD');
	var modifier = {$inc: {}};
	modifier.$inc['stats.registrations.' + date] = 1;
	model.findByIdAndUpdate(id, modifier, callback);
}

schema.statics.checkin = function(id, callback) {
	var date = moment().format('YYYYMMDD');
	var modifier = {$inc: {}};
	modifier.$inc['stats.checkins.' + date] = 1;
	modifier.$inc['stats.totals.checkins'] = 1;
	model.findByIdAndUpdate(id, modifier, callback);
}

schema.statics.subscribe = function(id, subscription, callback) {
	var date = moment().format('YYYYMMDD');
	var modifier = {$inc: {}};
	modifier.$inc['stats.money.' + date] = subscription.price;
	modifier.$inc['stats.totals.money'] = subscription.price;
	modifier.$inc['stats.totals.subscriptions'] = 1;
	model.findByIdAndUpdate(id, modifier, callback);
}

var model = mongoose.model('club', schema);
module.exports = model;