var mongoose = require('mongoose');
var helpers = require('../core/helpers');
var _ = require('lodash');
var moment = require('moment');

var schema = new mongoose.Schema({
	name: String,
	surname: String,
	email: String,
	phone: String,
	clubId: String,
	activeSubscription: Object,
	lastCheckin: Date,
	prepaidSubscriptions: {type: Array, default: []},
	subscriptions: {type: Array, default: []},
	history: Array,
	payments: Array,
	date: {type: Date, default: Date.now},
	stats: {
		type: Object, default: {
			moneySpent: 0,
			subscriptionsBoth: 0,
			checkins: 0,
			days: {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0},
			time: {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 0, 12: 0,
				13: 0, 14: 0, 15: 0, 16: 0, 17: 0, 18: 0, 19: 0, 20: 0, 21: 0, 22: 0, 23: 0}
		}
	}
});
schema.statics.checkin = function(id, callback) {
	model.findById(id, checkin);
	function checkin(err, client) {
		var sub = client.activeSubscription;
		if (!model.isActive(sub)) {
			if (client.prepaidSubscriptions.length > 0) {
				sub = client.prepaidSubscriptions.pull();
			} else {
				return callback({error: 'invalid subscription'});
			}
		}
		if (sub.type === 2 || sub.type === 3) {
			client.set('activeSubscription.lessonsCount', client.activeSubscription.lessonsCount - 1);
		}
		client.save(updateStatistic);
	}
	
	function updateStatistic(err) {
		if (err) return callback(err);
		var date = new Date();
		var day = date.getDay() * 1;
		var hour = date.getHours() * 1;
		if (day == 0) day = 7;

		var modifier = {$set: {}, $inc: {}};
		modifier.$inc['stats.time.' + hour] = 1;
		modifier.$inc['stats.days.' + day] = 1;
		modifier.$inc['stats.checkins'] = 1;
		modifier.$set.lastCheckin = date;
		model.findByIdAndUpdate(id, modifier, callback);
	}
}

schema.statics.subscribe = function(id, subscription, callback) {
	subscription = _.extend(_.omit(subscription.toJSON(), '__v', '_id'), {date: new Date});
	model.findById(id, checkin);
	function checkin(err, client) {
		if (model.isActive(client.activeSubscription)) {
			client.prepaidSubscriptions.push(subscription);
		} else {
			client.activeSubscription = subscription;
		}
		client.subscriptions.push(subscription);
		client.save(updateStatistic);
	}
	function updateStatistic(err) {
		var modifier = {
			$inc: {
				'stats.moneySpent': subscription.price,
				'stats.subscriptionsBoth': 1
			}
		};
		model.findByIdAndUpdate(id, modifier, callback);	
	}
}

schema.statics.isActive = function(sub) {
	if (!sub) return false;
	var monthesDifference = moment().diff(sub.date, 'monthes');
	var lessonsCount = sub.lessonsCount;
	var result = false;
	switch (sub.type) {
		case 1: return monthesDifference > sub.monthesCount; break;
		case 2: return lessonsCount > 0; break;
		case 3: return monthesDifference > sub.monthesCount && lessonsCount > 0; break;
	}
	return false;
}

var model = mongoose.model('client', schema);
module.exports = model;