var mongoose = require('mongoose');
var helpers = require('../core/helpers');

var schema = new mongoose.Schema({
	clientId: String,
	clubId: String,
	date: {type: Date, default: Date.now}
});

var model = mongoose.model('checkin', schema);
module.exports = model;