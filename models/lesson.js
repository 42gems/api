var mongoose = require('mongoose');

var schema = new mongoose.Schema({
	clubId: String,
	clients: Array,
	coachId: String,
	roomId: String,
	startTime: Date,
	endTime: Date,
	description: String,
	date: Date,
	siblings: Array
});

var model = mongoose.model('lesson', schema);

module.exports = model;