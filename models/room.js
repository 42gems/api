var mongoose = require('mongoose');

var schema = new mongoose.Schema({
	clubId: String,
	name: {type: String, required: true}
});

module.exports = mongoose.model('room', schema);