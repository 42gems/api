var mongoose = require('mongoose');

var schema = new mongoose.Schema({
	clubId: String
});

var model = mongoose.model('schedule', schema);

module.exports = model;