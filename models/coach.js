var mongoose = require('mongoose');

var schema = new mongoose.Schema({
	clubId: String,
	name: {type: String, required: true},
	specialization: String,
	description: String,
	phone: String,
	email: String,
	photo: String
});

module.exports = mongoose.model('coach', schema);