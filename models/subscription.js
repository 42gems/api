var mongoose = require('mongoose');

var schema = new mongoose.Schema({
	clubId: String,
	name: {type: String, required: true},
	price: Number,
	type: Number,
	monthesCount: Number,
	lessonsCount: Number
});

var model = mongoose.model('subscription', schema);

module.exports = model;