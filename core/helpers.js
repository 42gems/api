var _ = require('lodash');
var mongoose = require('mongoose');
module.exports = {
	randomString: function (string_length) {
		var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
		var s = '';
		if (string_length === undefined)
			string_length = 16;
		for (var i = 0; i < string_length; i++) {
			var rnum = Math.floor(Math.random() * chars.length);
			s += chars.substring(rnum, rnum + 1);
		}
		return s;
	},

	simpleOkResponse: function (res, err) {
		if (err) {
			return res.json(err);
		}
		return res.status(200).end();
	},

	simpleCollectionResponse: function (res, collectionName, err, collection) {
		if (err) {
			return res.json(err);
		}
		var result = {};
		result[collectionName] = [];
		for (var i = 0, l = collection.length; i < l; i++) {	
			result[collectionName].push(simplifyObject(collection[i]));
		}
		return res.json(result);
	},

	simpleObjectResponse: function (res, modelName, err, object) {
		if (err) {
			return res.json(err);
		}
		var result = {};
		result[modelName] = simplifyObject(object);
		return res.json(result);
	},

	simpleCreatedResponse: function (res, modelName, err, object) {
		if (err) {
			return res.json(err);
		}
		var result = {};
		result[modelName] = simplifyObject(object);
		return res.status(201).json(result);
	}
}

function simplifyObject(object) {
	if (object instanceof mongoose.Document) {
		object = object.toJSON();
	}
	if (object._id) {
		object.id = object._id;
	}
	if (object.password) {
		delete object.password;
	}
	if (object.salt) {
		delete object.salt;
	}
	return _.omit(object, '_id', '__v');
}