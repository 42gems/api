var expect = require('chai').expect,
	request = require('request').defaults({json: true}),
	config = require('../../config'),
	url = config.url + '/coaches',
	utils = require('../utils'),
	_ = require('lodash'),
	clubId = config.credentials.clubId;

describe('Coaches API', function () {
	var headers = {};
	var coach = {
		name: 'first coach',
		specialization: 'bass guitar',
		description: 'expirienced muzician',
		phone: '09324124',
		email: 'bass@rock.com',
		photo: 'http://coooooolbas.com/yeah.jpg'
	};
	var result;

	before(function(done) {
		utils.createUser(headers, done);
	});

	describe('create coach', function() {
		before(function(done) {
			request.post({
				url: url,
				headers: headers,
				body: {coach: coach}
			}, save.bind({}, done));
		});

		it('should return status code 201', function() {
			expect(result).to.have.property('statusCode').equals(201);
		});

		it('should return coach object', function() {
			expect(result)
				.to.have.property('body')
				.to.have.property('coach')
				.to.have.property('id');
		});

		it('should have auto setted clubId', function() {
			expect(result)
				.to.have.property('body')
				.to.have.property('coach')
				.to.have.property('clubId')
				.to.equals(clubId);
		});

		it('should return the same coach object', function() {
			var r = _.clone(result.body.coach);
			delete r.id;
			delete r.clubId;
			expect(r).to.deep.equal(coach);
		});

		describe('get coach by id', function() {
			var createdRoom;
			before(function(done) {
				createdRoom = result.body.coach;
				request.get({
					url: url + '/' + createdRoom.id,
					headers: headers
				}, save.bind({}, done));
			});

			it('should return status code 200', function() {
				expect(result).to.have.property('statusCode').equals(200);
			});

			it('should return the same coach object', function() {
				expect(result)
					.to.have.property('body')
					.to.have.property('coach')
					.to.deep.equal(createdRoom);
			});
		});
	})

	function save(done, err, response) {
		if (err) return done(err);
		result = response;
		done();
	}
});