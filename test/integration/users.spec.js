var expect = require('chai').expect,
	request = require('request').defaults({json: true}),
	config = require('../../config'),
	url = config.url + '/users',
	email = new Date() + config.credentials.email,
	password = config.credentials.password;

describe('Users API', function () {
	var result;
	describe('create user', function() {
		var user = {
			email: email,
			password: password
		};

		before(function(done) {
			request.post({
				url: url,
				body: {user: user}
			}, save.bind({}, done));
		});

		it('should have status code 201', function() {
			expect(result).to.have.property('statusCode').equals(201);
		});

		describe('then try to authorize', function() {
			var token;
			before(function(done) {
				request.post({
					url: url + '/auth',
					body: user
				}, save.bind({}, done));
			});

			it('should have status code 200', function() {
				expect(result).to.have.property('statusCode').equals(200);
			});

			it('should return token', function() {
				expect(result).to.have.property('body').to.have.property('token').to.be.ok;
				token = result.body.token;
			});

			describe('then try to change password', function() {

				var newPassword = '123123123';
				before(function(done) {
					request.put({
						url: url + '/changepassword',
						body: {
							password: password,
							newPassword: newPassword
						},
						headers: {
							'x-auth-token': token
						}
					}, save.bind({}, done));
				});

				it('should have status code 200', function() {
					expect(result).to.have.property('statusCode').equals(200);
				});

				describe('then try to login with new password', function() {
					before(function(done) {
						request.post({
							url: url + '/auth',
							body: {
								email: user.email,
								password: newPassword
							}
						}, save.bind({}, done));
					});

					it('should have status code 200', function() {
						expect(result).to.have.property('statusCode').equals(200);
					});

					it('should return token', function() {
						expect(result).to.have.property('body').to.have.property('token').to.be.ok;
					});
				});
			});
		});
	});

	function save(done, err, response) {
		if (err) return done(err);
		result = response;
		done();
	}
});