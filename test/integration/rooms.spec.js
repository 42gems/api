var expect = require('chai').expect,
	request = require('request').defaults({json: true}),
	config = require('../../config'),
	url = config.url + '/rooms',
	utils = require('../utils'),
	_ = require('lodash'),
	clubId = config.credentials.clubId;

describe('Rooms API', function () {
	var headers = {};
	var room = {
		name: 'first room'
	};
	var result;

	before(function(done) {
		utils.createUser(headers, done);
	});

	describe('create room', function() {
		before(function(done) {
			request.post({
				url: url,
				headers: headers,
				body: {room: room}
			}, save.bind({}, done));
		});

		it('should return status code 201', function() {
			expect(result).to.have.property('statusCode').equals(201);
		});

		it('should return room object', function() {
			expect(result)
				.to.have.property('body')
				.to.have.property('room')
				.to.have.property('id');
		});

		it('should have auto setted clubId', function() {
			expect(result)
				.to.have.property('body')
				.to.have.property('room')
				.to.have.property('clubId')
				.to.equals(clubId);
		});

		it('should return the same room object', function() {
			var r = _.clone(result.body.room);
			delete r.id;
			delete r.clubId;
			expect(r).to.deep.equal(room);
		});

		describe('get room by id', function() {
			var createdRoom;
			before(function(done) {
				createdRoom = result.body.room;
				request.get({
					url: url + '/' + createdRoom.id,
					headers: headers
				}, save.bind({}, done));
			});

			it('should return status code 200', function() {
				expect(result).to.have.property('statusCode').equals(200);
			});

			it('should return the same room object', function() {
				expect(result)
					.to.have.property('body')
					.to.have.property('room')
					.to.deep.equal(createdRoom);
			});
		});
	})

	function save(done, err, response) {
		if (err) return done(err);
		result = response;
		done();
	}
});