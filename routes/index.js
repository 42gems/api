var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
  res.end('<h1>API hcrm works</h1>');
});

var routes = {
	index: router,
	users: require('./users'),
	subscriptions: require('./subscriptions'),
	clients: require('./clients'),
	checkins: require('./checkins'),
	stats: require('./stats'),
	rooms: require('./rooms'),
	coaches: require('./coaches')
}

module.exports = routes;
