var express = require('express');
var controller = require('../controllers/users');
var middleware = require('../middleware');

module.exports = express.Router()
	.post('/', controller.create)
	.post('/auth', controller.auth)
	.put('/changepassword', middleware.authUser, middleware.checkAccess, controller.changepassword);