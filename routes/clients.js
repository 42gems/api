var express = require('express');
var middleware = require('../middleware');
var helpers = require('../core/helpers');
var controller = require('../controllers/clients')

module.exports = express.Router()
	.post('/', middleware.authUser, middleware.checkAccess, controller.create)
	.get('/', middleware.authUser, middleware.checkAccess, controller.getAll)
	.get('/:id', middleware.authUser, middleware.checkAccess, controller.getById)
	.put('/:id', middleware.authUser, middleware.checkAccess, controller.update)
	.put('/:id/subscribe', middleware.authUser, middleware.checkAccess, controller.subscribe);
