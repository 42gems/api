var express = require('express');
var middleware = require('../middleware');
var helpers = require('../core/helpers');
var controller = require('../controllers/checkins');
var User = require('../models/user');

module.exports = express.Router()
	.post('/',
		middleware.authUser, 
		middleware.checkAccess({roles: [User.ROLE_OWNER, User.ROLE_ADMIN]}),
		controller.create);
