var express = require('express');
var middleware = require('../middleware');
var helpers = require('../core/helpers');
var controller = require('../controllers/stats')

module.exports = express.Router()
	.get('/', middleware.authUser, middleware.checkAccess, controller.getAll);
