var _ = require('lodash');
var Checkin = require('../models/checkin');
var Client = require('../models/client');
var Club = require('../models/club');
var helpers = require('../core/helpers');
var controller = {
	create: function(req, res) {
		var clubId = req.getUser().getData().clubId;
		var clientId = req.body.checkin.clientId;
		var checkin = new Checkin({
			clientId: clientId,
			clubId: clubId
		});
		Client.checkin(clientId, saveCheckinToClub);
		function saveCheckinToClub(err) {
			if (err) return res.json(err);
			Club.checkin(clubId, saveCheckin)
		}
		function saveCheckin(err, client) {
			if (err && err.error == "invalid subscription") {
				return res.status(402).end();
			}
			if (err) {
				return res.json(err);
			}
			checkin.save(helpers.simpleCreatedResponse.bind({}, res, 'checkin'));
		}
	}
};
module.exports = controller;