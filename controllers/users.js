var Session = require('../core/session');
var User = require('../models/user');
var helpers = require('../core/helpers');

var controller = {
	auth: function(req, res) {
		var email = req.body.email,
			password = req.body.password;
		if (!email || !password) {
			return res.status(400).json({message: 'Емейл и пароль обязательные поля'});
		}
		email = email.toLowerCase().trim();

		User.findOne({email: email}, checkPassword);

		function checkPassword(err, user) {
			if (err) return res.json(err);
			if (!user || !User.passwordIsValid(user, password)) return res.status(401).end();
			var session = new Session();
			session.set('userId', user._id.toString());
			session.set('clubId', user.clubId);
			session.set('role', user.role);
			session.save(function(err, obj) {
				if (err) return res.json(err);
				return res.json({token: obj.token});
			});
		}
	},

	create: function(req, res) {
		var data = req.body.user;
		var email = data.email.toLowerCase().trim(),
			clubId = data.clubId,
			password = data.password;
		var user = new User({
			email: email,
			password: password,
			clubId: clubId
		});
		user.save(helpers.simpleCreatedResponse.bind({}, res, 'user'));
	},

	changepassword: function(req, res) {
		var userId = req.getUser().getData().userId;
		var data = req.body;
		var password = data.password;
		var newPassword = data.newPassword;
		User.findById(userId, checkPassword);

		function checkPassword(err, user) {
			if (err) return res.json(err);
			if (!user) return res.status(404).end();
			if (!User.passwordIsValid(user, password)) return res.status(401).end();
			user.password = newPassword;
			user.save(helpers.simpleOkResponse(res));
		}
	}
}
module.exports = controller;