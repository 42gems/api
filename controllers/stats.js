var _ = require('lodash');
var async = require('async');
var moment = require('moment');
var Club = require('../models/club');
var Client = require('../models/client');
var helpers = require('../core/helpers');
var controller = {
	getAll: function(req, res) {
		var clubId = req.getUser().getData().clubId;
		var result = { general: {}, charts: {}};
		var date = moment().format('YYYYMMDD');
		var monthStart = moment().startOf('month').format('YYYYMMDD');
		var lastMonthStart = moment().startOf('month').subtract('month', 1).format('YYYYMMDD');
		if (!clubId) {
			return res.status(500).end();
		}
		async.waterfall([
			getStats,
			totalClients,
			inactiveClients,
			activeClients,
			newClients
			], function() {
				res.json(result);
			});

		function getStats(callback) {
			Club.findOne().select('stats').where('_id').equals(clubId).exec(callback);
		}
		
		function totalClients(stats, callback) {
			if (stats) {
				result.charts.money = stats.stats.money;
				result.charts.checkins = stats.stats.checkins;
				result.charts.registrations = stats.stats.registrations;
				result.general = stats.stats.totals;
				result.general.money = {};
			}
			result.general.money.today = result.charts.money[date];

			data = _.pairs(result.charts.money).filter(function(m) {
				return m[0] >= monthStart;
			});
			if (data.length) {
				result.general.money.thisMonth = data.reduceRight(function(pv, cv) {
					return pv + cv[1]*1;
				}, 0);
			}

			data = _.pairs(result.charts.money).filter(function(m) {
				return m[0] < monthStart && m[0] >= lastMonthStart;
			});

			if (data.length) {
				result.general.money.thisMonth = data.reduceRight(function(pv, cv) {
					return pv + cv[1]*1;
				}, 0);
			}
			Client.count().where('clubId').equals(clubId).exec(callback);
		}

		function inactiveClients(total, callback) {
			result.general.allClients = total;
			Client.count()
				.where('clubId').equals(clubId)
				.where('lastCheckin').lt(moment().subtract('weeks', 2).toDate()).exec(callback);	
		}

		function activeClients(inactiveClients, callback) {
			result.general.skipedTwoWeeks = inactiveClients;
			Client.count()
				.where('clubId').equals(clubId)
				.where('lastCheckin').gt(moment().startOf('month').toDate()).exec(callback);
		}

		function newClients(active, callback) {
			result.general.activeClients = active;
			Client.count()
				.where('clubId').equals(clubId)
				.where('date').lt(moment().startOf('day').toDate()).exec(callback);
		}

		function saveNewCLients(clients, callback) {
			result.general.newClients = clients;
			callback();
		}
	}
};
module.exports = controller;