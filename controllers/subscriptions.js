var _ = require('lodash');
var Subscription = require('../models/subscription');
var helpers = require('../core/helpers');

var controller = {
	getAll: function(req, res) {
		var clubId = req.getUser().getData().clubId;
		Subscription
			.find()
			.where('clubId')
			.equals(clubId)
			.exec(helpers.simpleCollectionResponse.bind({}, res, 'subscriptions'));
	},
	getById: function(req, res) {
		var id = req.params.id;
		Subscription.findById(id, helpers.simpleObjectResponse.bind({}, res, 'subscription'));
	},
	create: function(req, res) {
		var clubId = req.getUser().getData().clubId;
		var data = _.pick(req.body.subscription, 'name', 'price', 'type', 'lessonsCount', 'monthesCount');
		data.clubId = clubId;
		var sub = new Subscription(data);
		sub.save(helpers.simpleCreatedResponse.bind({}, res, 'subscription'));
	},
	update: function(req, res) {
		var id = req.params.id;
		var data = _.pick(req.body.subscription, 'name', 'price', 'type', 'lessonsCount', 'monthesCount');
		var clubId = req.getUser().getData().clubId;
		Subscription.findById(id, checkOwner);

		function checkOwner(err, sub) {
			if (err) return res.status(500).json(err);
			if (!sub) return res.status(404);
			if (sub.clubId != clubId) return res.status(401);
			sub = _.extend(sub, data);
			Subscription.findByIdAndUpdate(id, {$set: data}, helpers.simpleObjectResponse.bind({}, res, 'subscription'));
		}
	},
	delete: function(req, res) {
		var id = req.params.id;
		var clubId = req.getUser().getData().clubId;
		var sub = Subscription.findById(id, checkOwner);

		function checkOwner(err, sub) {
			if (err) return res.status(500).json(err);
			if (!sub) return res.status(404);
			if (sub.clubId != clubId) return res.status(401);
			Subscription.findByIdAndRemove(id, helpers.simpleOkResponse.bind({}, res));
		}
	}
};
module.exports = controller;