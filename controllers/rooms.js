var _ = require('lodash');
var Room = require('../models/room');
var helpers = require('../core/helpers');
var controller = {
	getAll: function(req, res) {
		var clubId = req.getUser().getData().clubId;
		var query = Room
			.find()
			.where('clubId')
			.equals(clubId);
		query.exec(helpers.simpleCollectionResponse.bind({}, res, 'rooms'));
	},

	getById: function(req, res) {
		var id = req.params.id;
		Room.findById(id, helpers.simpleObjectResponse.bind({}, res, 'room'));
	},

	create: function(req, res) {
		var data = {
			clubId: req.getUser().getData().clubId,
			name: req.body.room.name
		};
		Room.create(data, helpers.simpleCreatedResponse.bind({}, res, 'room'));
	},

	update: function(req, res) {
		var id = req.params.id;
		var data = req.body.room;
		var clubId = req.getUser().getData().clubId;
		Room.findById(id, checkOwner);

		function checkOwner(err, room) {
			if (err) return res.status(500).json(err);
			if (!room) return res.status(404);
			if (room.clubId != clubId) return res.status(401);
			Room.update({_id: id}, {$set: data}, helpers.simpleObjectResponse.bind({}, res, 'room'));
		}
	},

	delete: function(req, res) {
		var id = req.params.id;
		var clubId = req.getUser().getData().clubId;
		Room.findById(id, checkOwner);

		function checkOwner(err, room) {
			if (err) return res.status(500).json(err);
			if (!room) return res.status(404);
			if (room.clubId != clubId) return res.status(401);
			Room.findByIdAndRemove(id, helpers.simpleOkResponse.bind({}, res));
		}
	}
};
module.exports = controller;