var _ = require('lodash');
var Coach = require('../models/coach');
var helpers = require('../core/helpers');
var controller = {
	getAll: function(req, res) {
		var clubId = req.getUser().getData().clubId;
		var query = Coach
			.find()
			.where('clubId')
			.equals(clubId);
		query.exec(helpers.simpleCollectionResponse.bind({}, res, 'coaches'));
	},

	getById: function(req, res) {
		var id = req.params.id;
		Coach.findById(id, helpers.simpleObjectResponse.bind({}, res, 'coach'));
	},

	create: function(req, res) {
		var data = req.body.coach;
		data.clubId = req.getUser().getData().clubId;
		Coach.create(data, helpers.simpleCreatedResponse.bind({}, res, 'coach'));
	},

	update: function(req, res) {
		var id = req.params.id;
		var data = req.body.coach;
		var clubId = req.getUser().getData().clubId;
		Coach.findById(id, checkOwner);

		function checkOwner(err, coach) {
			if (err) return res.status(500).json(err);
			if (!coach) return res.status(404);
			if (coach.clubId != clubId) return res.status(401);
			Coach.update({_id: id}, {$set: data}, helpers.simpleObjectResponse.bind({}, res, 'coach'));
		}
	},

	delete: function(req, res) {
		var id = req.params.id;
		var clubId = req.getUser().getData().clubId;
		Coach.findById(id, checkOwner);

		function checkOwner(err, coach) {
			if (err) return res.status(500).json(err);
			if (!coach) return res.status(404);
			if (coach.clubId != clubId) return res.status(401);
			Coach.findByIdAndRemove(id, helpers.simpleOkResponse.bind({}, res));
		}
	}
};
module.exports = controller;