var _ = require('lodash');
var Subscription = require('../models/subscription');
var Client = require('../models/client');
var Club = require('../models/club');
var User = require('../models/user');
var helpers = require('../core/helpers');
var controller = {
	getAll: function(req, res) {
		var clubId = req.getUser().getData().clubId;
		var term = req.query.term;
		var query = Client
			.find()
			.select('_id name surname email activeSubscription stats')
			.sort({lastCheckin: -1})
			.where('clubId')
			.equals(clubId);
		if (term) {
			term = '^' + term;
			query.or([
				{name: {$regex: term, $options: '-i'}},
				{surname: {$regex: term, $options: '-i'}},
				{phone: {$regex: term, $options: '-i'}},
				{email: {$regex: term, $options: '-i'}}
				]);
		}
		query.exec(populateActivity);

		function populateActivity(err, clients) {
			clients = clients.map(getActivity);
			res.json({clients: clients});
		}

		function getActivity(client) {
			client = client.toJSON();
			client.id = client._id;
			delete client._id;
			client.expired = !Client.isActive(client.activeSubscription);
			return client;
		}
	},

	getById: function(req, res) {
		var id= req.params.id;
		Client.findById(id, helpers.simpleObjectResponse.bind({}, res, 'client'));
	},

	create: function(req, res) {
		var clubId = req.getUser().getData().clubId;
		var data = _.pick(req.body.client, 'name', 'surname', 'email', 'phone');
		data.clubId = clubId;
		var client = new Client(data);
		Club.newClient(clubId, client.save.bind({}, checkUserExistance));
		
		function checkUserExistance(err, client) {
			if (err) return res.status(500).json(err);
			User.findOne({email: data.email}, function(err, user) {
				if (err) return res.status(500).json(err);
				if (!user) {
					createUser(client, helpers.simpleCreatedResponse.bind({}, res, 'client', null, client));
				} else {
					helpers.simpleCreatedResponse(res, 'client', null, client);
				}
			});
		}

		function createUser(client, callback) {
			var user = new User(data);
			var password = helpers.randomString(7);
			user.clients = [client._id.toString()];
			user.salt = helpers.randomString(30);
			user.password = User.generatePassword(user.salt, password);
			user.role = User.ROLE_USER;
			user.save(callback);
			// send letter
		}
	},

	update: function(req, res) {
		var id = req.params.id;
		var data = _.pick(req.body, 'name', 'surname', 'email', 'phone');
		var clubId = req.getUser().getData().clubId;
		var client = Client.findById(id, checkOwner);

		function checkOwner(err, client) {
			if (err) return res.status(500).json(err);
			if (!client) return res.status(404);
			if (client.clubId != clubId) return res.status(401);
			Client.update({_id: id}, {$set: data}, helpers.simpleObjectResponse.bind({}, res, 'client'));
		}
	},

	subscribe: function(req, res) {
		var id = req.params.id;
		var subscriptionId = req.body.subscriptionId;
		var clubId = req.getUser().getData().clubId;
		if (!subscriptionId) return res.status(400).end();
		Subscription.findById(subscriptionId, checkOwner);

		function checkOwner(err, subscription) {
			if (err) return res.status(500).json(err);
			if (subscription.clubId != clubId) return res.status(401);
			Client.subscribe(id, subscription, function(err) {
				if (err) return res.status(500).json(err);
				Club.subscribe(clubId, subscription, helpers.simpleOkResponse.bind({}, res));
			});
		}
	}
};
module.exports = controller;